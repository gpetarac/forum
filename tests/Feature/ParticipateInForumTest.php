<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class ParticipateInForumTest extends TestCase
{

    use DatabaseMigrations;


    /**
     * @test
     */
    public function unauthenticated_users_may_not_add_replies()
    {
        $this->withExceptionHandling();
        $thread = create('App\Thread');
        $this->post($thread->path() . '/replies/' , [])
            ->assertRedirect('/login');


    }


    /**
     * @test
     */
    public function an_authenticated_user_may_participate_in_forum_threads()
    {

        $this->signIn();

        $thread = create('App\Thread');

        $reply = make('App\Replay');

        $this->post($thread->path() . '/replies/' , $reply->toArray());

        $this->get($thread->path())
            ->assertSee($reply->body);
    }

    /**
     * @test
     */
    public function a_replay_requires_a_body()
    {

        $this->withExceptionHandling()->signIn();

        $thread = create('App\Thread');

        $reply = make('App\Replay', ['body' => null]);

        $this->post($thread->path() . '/replies/' , $reply->toArray())
            ->assertSessionHasErrors('body');
    }
}
